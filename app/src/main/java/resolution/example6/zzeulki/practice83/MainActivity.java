package resolution.example6.zzeulki.practice83;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private EditText e;
    private String today;
    static final int G_NOTIFY_NUM = 1;
    NotificationManager m_NotiManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        m_NotiManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        e = (EditText) this.findViewById(R.id.secText);
    }

    public void click(View v)
    {
        Calendar cal = Calendar.getInstance();
        java.util.Date date = cal.getTime();

       today = (new SimpleDateFormat("yyyyMMddHHmmss").format(date));
        if(v.getId()==R.id.button)
        {
            v.postDelayed(new Runnable() {
                @Override
                public void run() {
                        int icon = MainActivity.this.getApplicationInfo().icon;
                        Intent intent = new Intent(MainActivity.this,MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        PendingIntent content = PendingIntent.getActivity(MainActivity.this,0,intent,0);
                        Notification noti = new Notification.Builder(MainActivity.this).setContentTitle("현재시간")
                                .setContentText(today.toString())
                                .setSmallIcon(icon)
                                .setContentIntent(content)
                                .setWhen(System.currentTimeMillis())
                                .build();
                        m_NotiManager.notify(MainActivity.G_NOTIFY_NUM,noti);
                   }
            }, Integer.parseInt(e.getText().toString())*1000);
        }
    }
}
